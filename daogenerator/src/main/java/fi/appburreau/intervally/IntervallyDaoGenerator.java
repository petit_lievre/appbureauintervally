package fi.appburreau.intervally;


import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Schema;


public class IntervallyDaoGenerator {

    public static void main(String args[]) throws Exception {
        Schema schema = new Schema(1, "fi.appbureau.intervally.model");

        Entity timer = schema.addEntity("Timer");
        timer.addIdProperty();
        timer.addStringProperty("name");
        timer.addIntProperty("warmUp");
        timer.addIntProperty("exercise");
        timer.addIntProperty("rest");
        timer.addIntProperty("recovery");
        timer.addIntProperty("numberOfExercises");
        timer.addIntProperty("rounds");
        timer.addIntProperty("coolDown");
        timer.implementsSerializable();

        new DaoGenerator().generateAll(schema, "app/src/main/java");
    }
}
