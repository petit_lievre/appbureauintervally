package fi.appbureau.intervally.service;


import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.LinkedList;
import java.util.List;

import fi.appbureau.intervally.model.Timer;
import fi.appbureau.intervally.notification.RunningTimerNotification;


public class TimerService extends Service {

    private static final String TAG = TimerService.class.getSimpleName();

    public static final int INITIAL_STATE = 0;
    public static final int SCHEDULED_STATE = 1;
    public static final int RUNNING_STATE = 2;
    public static final int PAUSED_STATE = 3;
    public static final int STOPPED_STATE = 4;

    public static final String ACTION_TOGGLE = TAG + ".ACTION_TOGGLE";

    public class LocalBinder extends Binder {

        public TimerService getService() {
            return TimerService.this;
        }
    }

    private final IBinder mBinder = new LocalBinder();

    private final TimerDelegate activeTimerDelegate = new TimerDelegate() {
        @Override
        public void onActiveTimerChanged(long total, long totalLeft,
                                         long interval, long intervalLeft, int type) {
            for(TimerDelegate delegate : mDelegatesList) {
                delegate.onActiveTimerChanged(total, totalLeft, interval, intervalLeft, type);
            }
        }

        @Override
        public void onActiveTimerStarted(long secondsLeftTotal, long secondLeftInterval) {
             for(TimerDelegate delegate : mDelegatesList) {
                    delegate.onActiveTimerStarted(secondsLeftTotal, secondLeftInterval);
                }

        }

        @Override
        public void onActiveTimerStartScheduled(int timeTillStart) {
            for(TimerDelegate delegate : mDelegatesList) {
                delegate.onActiveTimerStartScheduled(timeTillStart);
            }
        }

        @Override
        public void onActiveTimerPaused() {
            for(TimerDelegate delegate : mDelegatesList) {
                delegate.onActiveTimerPaused();
            }
        }

        @Override
        public void onActiveTimerStopped() {
            for(TimerDelegate delegate : mDelegatesList) {
                delegate.onActiveTimerStopped();
            }
                mActiveTimer = null;
                mRunningTimer = null;
        }
    };

    private @Nullable ActiveTimer mActiveTimer;

    private final List<TimerDelegate> mDelegatesList = new LinkedList<>();

    private @Nullable Timer mRunningTimer;

    @Override
    public void onCreate() {
        super.onCreate();
        new RunningTimerNotification(this);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public void addActiveTimerDelegate(@NonNull TimerDelegate timerDelegate) {
        mDelegatesList.add(timerDelegate);
    }

    public void removeActiveTimerDelegate(@NonNull TimerDelegate timerDelegate) {
        mDelegatesList.remove(timerDelegate);
    }

    public void startTimer(Timer timer, int delay) {
        Log.d(TAG, "startTimer");
        mActiveTimer = new ActiveTimer(this, activeTimerDelegate, timer);
        mActiveTimer.start(delay);
        mRunningTimer = timer;
    }

    public void pauseTimer() {
        if (mActiveTimer != null) {
            mActiveTimer.pause();
        }
    }

    private void toggle() {
        if (mActiveTimer != null && mActiveTimer.getState() == RUNNING_STATE) {
            pauseTimer();
        } else {
            doContinueActiveTimer();
        }
    }

    public void doContinueActiveTimer() {
        if (mActiveTimer != null) {
            mActiveTimer.doContinue();
        }
    }

    public void stopTimer() {
        if (mActiveTimer != null) {
            mActiveTimer.stop();
            mActiveTimer = null;
            mRunningTimer = null;
        }
    }

    @Nullable
    public Timer getRunningTimer() {
        return mRunningTimer;
    }

    public int getState() {
        if (mActiveTimer == null) {
            return INITIAL_STATE;
        } else {
            return mActiveTimer.getState();
        }
    }

    public int getTotalLeft() {
        return (int)mActiveTimer.getTotalLeft();
    }

    public int getIntervalLeft() {
        return (int)mActiveTimer.getIntervalLeft();
    }

    public int getTotal() {
        return (int)mActiveTimer.getTotal();
    }

    public int getInterval() {
        return (int)mActiveTimer.getInterval();
    }

    public int getPhase() {
        return mActiveTimer.getPhase();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if(intent != null && ACTION_TOGGLE.equals(intent.getAction())) {
            toggle();
        }
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopTimer();
    }
}
