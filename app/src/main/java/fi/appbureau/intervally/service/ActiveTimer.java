package fi.appbureau.intervally.service;


import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Handler;

import java.util.LinkedList;

import fi.appbureau.intervally.R;
import fi.appbureau.intervally.model.Timer;
import fi.appbureau.intervally.model.TimerComponents;
import fi.appbureau.intervally.preferences.IntervallyPreferences;


public class ActiveTimer implements CountDownDelegate {

    private static final String TAG = ActiveTimer.class.getSimpleName();

    private final Context context;
    private final TimerDelegate listener;
    private final LinkedList<Interval> mIntervals = new LinkedList<>();

    private final IntervallyPreferences preferences;

    private int mActiveIntervalIndex = 0;

    /**
     * Corresponds to TimerDelegate.onActiveTimerChanged params
     * */
    private int mTotal;
    private int mTotalLeft;
    private int mInterval;
    private int mIntervalLeft;
    private int mType;

    private int mState;

    private final Handler handler = new Handler();

    public ActiveTimer(Context context, TimerDelegate listener, Timer timer) {
        this.context = context;
        this.listener = listener;

        preferences = new IntervallyPreferences(context);

        constructIntervals(timer);

        calculateTotal();

        mState = TimerService.INITIAL_STATE;
    }

    public int getState() {
        return mState;
    }

    private void constructIntervals(Timer timer) {
        if (timer.getWarmUp() != null) {
            mIntervals.add(new Interval(TimerComponents.WARM_UP, timer.getWarmUp(), this));
        }

        if(timer.getRounds() != null) {
            for (int i = 0; i < timer.getRounds(); i++) {
                if (timer.getNumberOfExercises() != null) {
                    for (int j = 0; j < timer.getNumberOfExercises(); j++) {

                        if (timer.getExercise() != null) {
                            mIntervals.add(new Interval(TimerComponents.EXERCISE, timer.getExercise(), this));
                        }

                        if (timer.getRest() != null && j != timer.getNumberOfExercises() - 1) {
                            mIntervals.add(new Interval(TimerComponents.REST, timer.getRest(), this));
                        }
                    }
                }
                if (timer.getRecovery() != null) {
                    mIntervals.add(new Interval(TimerComponents.RECOVERY, timer.getRecovery(), this));
                }
            }
        }

        if (timer.getCoolDown() != null) {
            mIntervals.add(new Interval(TimerComponents.COOL_DOWN, timer.getCoolDown(), this));
        }
    }

    public long getTotalLeft() {
        return  mTotalLeft;
    }

    public long getIntervalLeft() {
        return  mIntervalLeft ;
    }

    public long getTotal() {
        return mTotal;
    }

    public long getInterval() {
        return mInterval;
    }

    public int getPhase() {
        return mType;
    }

    @Override
    public void onCountDownFinish() {
        mActiveIntervalIndex++;

        if (mActiveIntervalIndex < mIntervals.size()) {
            startActiveInterval();
        } else {
            mState = TimerService.STOPPED_STATE;
            listener.onActiveTimerStopped();
        }
    }

    @Override
    public void onCountDownTick(int secondsUntilFinished) {
        Interval activeInterval = mIntervals.get(mActiveIntervalIndex);
        long diff = mIntervalLeft - secondsUntilFinished;
        mIntervalLeft = secondsUntilFinished;

        mTotalLeft -= diff;
        listener.onActiveTimerChanged(mTotal, mTotalLeft, mInterval, mIntervalLeft, mType);

        if (mActiveIntervalIndex != 0) {
            final int level = getLevelForType(activeInterval.type);
            Interval prevActiveInterval = mIntervals.get(mActiveIntervalIndex - 1);
            final int prevLevel = getLevelForType(prevActiveInterval.type);
            long r = activeInterval.getDurationSeconds() - secondsUntilFinished;
            if (r == 0 && prevLevel != level) {
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        int middle = (level + prevLevel) / 2;
                        setLevel(middle);
                    }
                }, 500);

                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        setLevel(level);
                    }
                }, 750);
            }
        }
    }

    private void calculateTotal() {
        mTotalLeft = 0;
        for (Interval interval : mIntervals) {
            mTotalLeft += interval.getDurationSeconds();
        }
        mTotal = mTotalLeft;
    }

    private void start() {
        mActiveIntervalIndex = 0;

        startActiveInterval();

        mState = TimerService.RUNNING_STATE;

        listener.onActiveTimerStarted(mTotal, mIntervalLeft);
    }

    public void start(int delay) {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                start();
            }
        }, delay * 1000);
        mState = TimerService.SCHEDULED_STATE;
        for (int i = 0; i <= delay; i++) {
            final int countDown = delay - i;
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    listener.onActiveTimerStartScheduled(countDown);
                }
            }, i * 1000);
        }
    }

    public void stop() {
        if (mActiveIntervalIndex < mIntervals.size()) {
            Interval activeInterval = mIntervals.get(mActiveIntervalIndex);
            activeInterval.stop();

            mState = TimerService.STOPPED_STATE;
        }

        mActiveIntervalIndex = 0;

        listener.onActiveTimerStopped();
    }

    public void pause() {
        Interval activeInterval = mIntervals.get(mActiveIntervalIndex);
        activeInterval.pause();

        mState = TimerService.PAUSED_STATE;

        listener.onActiveTimerPaused();
    }

    public void doContinue() {
        Interval activeInterval = mIntervals.get(mActiveIntervalIndex);
        activeInterval.doContinue();

        mState = TimerService.RUNNING_STATE;

        long secondsLeft = mTotalLeft;

        listener.onActiveTimerChanged(mTotal, secondsLeft, mInterval, mIntervalLeft, mType);
    }

    private void startActiveInterval() {
        Interval activeInterval = mIntervals.get(mActiveIntervalIndex);
        mType = activeInterval.type;
        mIntervalLeft = activeInterval.getDurationSeconds();
        mInterval = activeInterval.getDurationSeconds();
        if (mActiveIntervalIndex == 0) {
            int level = getLevelForType(activeInterval.type);
            setLevel(level);
            playAlert(activeInterval.type);
            activeInterval.start();
        } else {
            Interval prevActiveInterval = mIntervals.get(mActiveIntervalIndex - 1);
            int prevLevel = getLevelForType(prevActiveInterval.type);
            int level = getLevelForType(activeInterval.type);
            if (prevLevel < level) {
                setLevel(prevLevel + 1);
            } else {
                setLevel(prevLevel - 1);
            }
            playAlert(activeInterval.type);
            activeInterval.start();
        }
    }

    private int getLevelForType(int type) {
        switch (type) {
            case TimerComponents.WARM_UP:
                return preferences.getWarmUpVolumeLevel();
            case TimerComponents.EXERCISE:
                return preferences.getExerciseVolumeLevel();
            case TimerComponents.REST:
                return preferences.getRestVolumeLevel();
            case TimerComponents.RECOVERY:
                return preferences.getRecoveryVolumeLevel();
            case TimerComponents.COOL_DOWN:
                return preferences.getCoolDownVolumeLevel();
            default:
                return 0;
        }
    }

    private void playAlert(int type) {

        if (!preferences.getPlaySounds()) {
            return;
        }

        int alert;

        if (type == TimerComponents.EXERCISE) {
            alert = R.raw.beep_3x;
        } else {
            alert = R.raw.beep;
        }

        final MediaPlayer mediaPlayer = MediaPlayer.create(context.getApplicationContext(), alert);
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mediaPlayer.release();
            }
        });
        mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mediaPlayer.start();
            }
        });

    }

    private void setLevel(int level) {
        AudioManager audioManager =
                (AudioManager)context.getSystemService(Context.AUDIO_SERVICE);
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
                level, AudioManager.FLAG_VIBRATE);
    }
}
