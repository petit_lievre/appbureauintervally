package fi.appbureau.intervally.service;


import android.os.CountDownTimer;


class Interval {

    public final int type;
    private final CountDownDelegate countDownDelegate;

    private long mDurationInMilliseconds;
    private CountDownTimer mCountDownTimer;
    private long mMillisUntilFinished;

    private int mMillisUntilFinishedRound = 0;

    public Interval(int type, long durationInSeconds, final CountDownDelegate countDownDelegate) {
        this.type = type;

        mDurationInMilliseconds = durationInSeconds * 1000L;
        this.countDownDelegate = countDownDelegate;
        mCountDownTimer = createCountDownTimer();
    }

    public int getDurationSeconds() {
        return (int)(mDurationInMilliseconds / 1000L);
    }

    public void start() {
        mCountDownTimer.start();
    }

    public void pause() {
        mCountDownTimer.cancel();
        mDurationInMilliseconds = mMillisUntilFinished;
    }

    public void doContinue() {
        mCountDownTimer = createCountDownTimer();
        mCountDownTimer.start();
    }

    public void stop() {
        mCountDownTimer.cancel();
    }

    private CountDownTimer createCountDownTimer() {
        return new CountDownTimer(mDurationInMilliseconds, 100) {
            @Override
            public void onTick(long millisUntilFinished) {

                Interval.this.mMillisUntilFinished = millisUntilFinished;

                if (Math.round((float) millisUntilFinished / 1000.0f) != mMillisUntilFinishedRound) {

                    mMillisUntilFinishedRound = Math.round((float) millisUntilFinished / 1000.0f);

                    Interval.this.countDownDelegate.onCountDownTick(mMillisUntilFinishedRound);
                }
            }

            @Override
            public void onFinish() {
                Interval.this.countDownDelegate.onCountDownFinish();
            }
        };
    }
}
