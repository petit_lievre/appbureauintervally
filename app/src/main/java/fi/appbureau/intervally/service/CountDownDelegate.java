package fi.appbureau.intervally.service;


interface CountDownDelegate {

    void onCountDownFinish();

    void onCountDownTick(int secondsUntilFinished);

}
