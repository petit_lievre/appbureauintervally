package fi.appbureau.intervally.service;


public interface TimerDelegate {

    void onActiveTimerChanged(long total,
                              long totalLeft,
                              long interval,
                              long intervalLeft,
                              int type);

    void onActiveTimerStartScheduled(int timeTillStart);

    void onActiveTimerStarted(long total,
                              long interval);

    void onActiveTimerPaused();

    void onActiveTimerStopped();
}
