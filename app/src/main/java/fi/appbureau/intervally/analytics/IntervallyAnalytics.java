package fi.appbureau.intervally.analytics;


public class IntervallyAnalytics {

    public static final String EVENT_STARTED_TIMER = "TimerStarted";

    public static final String PARAM_TIMER = "TimerParam";
    public static final String PARAM_SETTINGS = "SettingsParam";
}
