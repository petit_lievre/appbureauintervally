package fi.appbureau.intervally.activity.catalog;


import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import fi.appbureau.intervally.R;
import fi.appbureau.intervally.activity.IntervallyActivity;
import fi.appbureau.intervally.model.Timer;
import fi.appbureau.intervally.model.TimerDaoHelper;
import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter;
import jp.wasabeef.recyclerview.animators.FadeInAnimator;
import jp.wasabeef.recyclerview.animators.FadeInUpAnimator;
import jp.wasabeef.recyclerview.animators.LandingAnimator;
import jp.wasabeef.recyclerview.animators.ScaleInAnimator;
import jp.wasabeef.recyclerview.animators.ScaleInTopAnimator;
import jp.wasabeef.recyclerview.animators.SlideInDownAnimator;
import jp.wasabeef.recyclerview.animators.SlideInLeftAnimator;
import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;


public class CatalogFragment extends Fragment implements CatalogAdapter.onTimerChangeListener, CatalogAdapter.onTimerStartListener {

    public static final String TAG = CatalogFragment.class.getSimpleName();

    private View mRootView;
    private CatalogAdapter mAdapter;

    public CatalogFragment() {
        // Required empty public constructor
    }

    public static CatalogFragment newInstance() {
        return new CatalogFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.fragment_catalog, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        RecyclerView timersList = (RecyclerView)view.findViewById(R.id.timersList);
        timersList.setLayoutManager(new LinearLayoutManager(getActivity()));
        timersList.setItemAnimator(new SlideInUpAnimator());
        timersList.getItemAnimator().setAddDuration(150);
        timersList.getItemAnimator().setRemoveDuration(150);
        timersList.getItemAnimator().setMoveDuration(150);
        timersList.getItemAnimator().setChangeDuration(150);
        Context context = getActivity() != null ? getActivity() : getContext();
        mAdapter = new CatalogAdapter(context, this, this);
        timersList.setAdapter(mAdapter);
        mRootView = view.findViewById(R.id.root_view);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_catalog, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onResume() {
        super.onResume();

        AppCompatActivity activity = (AppCompatActivity)getActivity();
        if (activity != null) {
            ActionBar actionBar = activity.getSupportActionBar();
            actionBar.setDisplayHomeAsUpEnabled(false);
            actionBar.setDisplayOptions(ActionBar.DISPLAY_USE_LOGO, 1);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add:
                showTimerBuilder(new Timer());
                return true;
            case R.id.action_settings:
                showSettings();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showTimerBuilder(Timer timer) {
        if (getActivity() != null) {
            IntervallyActivity activity = (IntervallyActivity) getActivity();
            activity.showTimerBuilder(timer);
        }
    }

    private void showSettings() {
        if (getActivity() != null) {
            IntervallyActivity activity = (IntervallyActivity) getActivity();
            activity.showSettings();
        }
    }

    @Override
    public void onTimerEdit(Timer timer) {
        showTimerBuilder(timer);
    }

    @Override
    public void onTimerDeleted(final Timer timer) {
        TimerDaoHelper.deleteTimer(this, timer);
        mAdapter.refresh();

        Snackbar undo = Snackbar.make(mRootView, R.string.timer_deleted, Snackbar.LENGTH_LONG);
        undo.setAction(R.string.undo, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (timer != null) {
                    TimerDaoHelper.insertTimer(CatalogFragment.this, timer);
                    mAdapter.refresh();
                }
            }
        });
        undo.setActionTextColor(Color.WHITE);
        Context context = getActivity() != null ? getActivity() : getContext();
        undo.getView().setBackgroundColor(ContextCompat.getColor(context, R.color.colorSnackbarError));
        undo.show();
    }

    @Override
    public void onStartTimer(Timer timer) {
        if (getActivity() != null) {
            IntervallyActivity activity = (IntervallyActivity) getActivity();
            activity.showRunningTimer(timer);
        }
    }
}
