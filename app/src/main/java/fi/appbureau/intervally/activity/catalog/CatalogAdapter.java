package fi.appbureau.intervally.activity.catalog;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.LinkedList;
import java.util.List;

import fi.appbureau.intervally.R;
import fi.appbureau.intervally.activity.util.FragmentUtils;
import fi.appbureau.intervally.model.Timer;
import fi.appbureau.intervally.model.TimerComponents;
import fi.appbureau.intervally.model.TimerDaoHelper;
import fi.appbureau.intervally.model.TimerUtils;


public class CatalogAdapter extends RecyclerView.Adapter<CatalogAdapter.TimerView> {

    private static final int UNDEFINED = -1;

    private int mExpandedItemPosition = UNDEFINED;

    public interface onTimerChangeListener {
        void onTimerEdit(Timer timer);
        void onTimerDeleted(Timer timer);
    }

    public interface onTimerStartListener {
        void onStartTimer(Timer timer);
    }

    private final Context context;
    private final onTimerChangeListener onChangeListener;
    private final onTimerStartListener onStartListener;

    private final List<Timer> mTimersList = new LinkedList<>();

    public static final class TimerView extends RecyclerView.ViewHolder {

        View itemContainerView;
        View legend;
        TextView name;
        TextView duration;
        ImageButton infoButton;
        ImageButton editButton;
        ImageButton deleteButton;
        ImageButton startButton;

        public TimerView(View itemView) {
            super(itemView);

            itemContainerView = itemView.findViewById(R.id.item_container);
            legend = itemView.findViewById(R.id.legend);

            name = (TextView) itemView.findViewById(R.id.name);
            duration = (TextView) itemView.findViewById(R.id.duration);

            infoButton = (ImageButton)itemView.findViewById(R.id.info);
            editButton = (ImageButton)itemView.findViewById(R.id.edit);
            deleteButton = (ImageButton)itemView.findViewById(R.id.delete);

            startButton = (ImageButton)itemView.findViewById(R.id.start);
        }
    }

    public CatalogAdapter(Context context, onTimerChangeListener onEditListener,
                          onTimerStartListener onStartListener) {

        this.context = context;
        this.onChangeListener = onEditListener;
        this.onStartListener = onStartListener;

        mTimersList.addAll(TimerDaoHelper.loadAll(context));
    }

    public void refresh() {
        mTimersList.clear();
        mTimersList.addAll(TimerDaoHelper.loadAll(context));
        notifyDataSetChanged();
    }

    @Override
    public CatalogAdapter.TimerView onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemLayoutView = inflater.inflate(R.layout.timers_list_item_view, null);

        return new TimerView(itemLayoutView);
    }

    @Override
    public void onBindViewHolder(CatalogAdapter.TimerView holder, int position) {

        final Timer timer = mTimersList.get(position);

        holder.name.setText(timer.getName());
        holder.duration.setText(TimerUtils.getTotalDuration(timer));

        configureLegendView(holder, timer);

        if (holder.getAdapterPosition() != mExpandedItemPosition) {
            holder.legend.setVisibility(View.GONE);
        } else {
            holder.legend.setVisibility(View.VISIBLE);
        }

        expandableLegendFor(holder.itemContainerView, holder);
        expandableLegendFor(holder.infoButton, holder);

        holder.editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onChangeListener.onTimerEdit(timer);
            }
        });

        holder.deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onChangeListener.onTimerDeleted(timer);
            }
        });

        holder.startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onStartListener.onStartTimer(timer);
            }
        });
    }


    private void configureLegendView(TimerView holder, Timer timer) {
        FragmentUtils.configureComponentView(holder.legend.findViewById(R.id.warm_up), timer,
                TimerComponents.WARM_UP, R.drawable.ic_warmup, R.string.warm_up, R.string.empty_field_time);

        FragmentUtils.configureComponentView(holder.legend.findViewById(R.id.exercise), timer,
                TimerComponents.EXERCISE, R.drawable.ic_exercise, R.string.exercise, R.string.empty_field_time);

        FragmentUtils.configureComponentView(holder.legend.findViewById(R.id.rest), timer,
                TimerComponents.REST, 0, R.string.rest, R.string.empty_field_time);

        FragmentUtils.configureComponentView(holder.legend.findViewById(R.id.recovery), timer,
                TimerComponents.RECOVERY, 0, R.string.recovery, R.string.empty_field_time);

        FragmentUtils.configureComponentView(holder.legend.findViewById(R.id.numberOfExercises), timer,
                TimerComponents.NUMBER_OF_EXERCISES, 0, R.string.number_of_exercises, R.string.empty_field_number);

        FragmentUtils.configureComponentView(holder.legend.findViewById(R.id.rounds), timer,
                TimerComponents.ROUNDS, 0, R.string.rounds, R.string.empty_field_number);

        FragmentUtils.configureComponentView(holder.legend.findViewById(R.id.coolDown), timer,
                TimerComponents.COOL_DOWN, R.drawable.ic_cooldown, R.string.cool_down, R.string.empty_field_time);
    }

    private void expandableLegendFor(View view, final CatalogAdapter.TimerView holder) {
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int visibility;
                if (holder.getAdapterPosition() == mExpandedItemPosition) {
                    mExpandedItemPosition = UNDEFINED;
                    visibility = View.GONE;

                } else {
                    notifyItemChanged(mExpandedItemPosition, mExpandedItemPosition);
                    mExpandedItemPosition = holder.getAdapterPosition();
                    visibility = View.VISIBLE;
                }

                holder.legend.setVisibility(visibility);
                notifyItemChanged(holder.getAdapterPosition(), holder.getAdapterPosition());
            }
        });
    }

    @Override
    public int getItemCount() {
        return mTimersList.size();
    }


}
