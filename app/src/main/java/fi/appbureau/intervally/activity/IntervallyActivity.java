package fi.appbureau.intervally.activity;


import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import fi.appbureau.intervally.R;
import fi.appbureau.intervally.activity.active.RunningTimerFragment;
import fi.appbureau.intervally.activity.assemble.AssembleFragment;
import fi.appbureau.intervally.activity.catalog.CatalogFragment;
import fi.appbureau.intervally.activity.settings.LicencesFragment;
import fi.appbureau.intervally.activity.settings.SettingsFragment;
import fi.appbureau.intervally.activity.splash.SplashFragment;
import fi.appbureau.intervally.model.Timer;
import fi.appbureau.intervally.service.TimerService;


public class IntervallyActivity extends AppCompatActivity implements SplashFragment.OnSplashShownListener {

    private static final String TAG = IntervallyActivity.class.getSimpleName();

    @Nullable private TimerService mTimerService;
    private boolean mIsServiceBound = false;
    private boolean mIsSplashShown = false;

    private Toolbar mToolbar;

    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            TimerService.LocalBinder binder = (TimerService.LocalBinder) service;
            mTimerService = binder.getService();
            mIsServiceBound = true;

            navigateAfterStarted();
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mIsServiceBound = false;
            mTimerService = null;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_intervally);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setLogo(R.drawable.ic_logo);

        Intent intent = new Intent(this, TimerService.class);
        startService(intent);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);

        showSplash();

        MobileAds.initialize(getApplicationContext(), "ca-app-pub-4167922603169903~6536973477");

        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.JELLY_BEAN) {
            outState.putString("WORKAROUND_FOR_BUG_19917_KEY", "WORKAROUND_FOR_BUG_19917_VALUE");
        }
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (mIsServiceBound) {
            unbindService(mConnection);
            mIsServiceBound = false;
        }
        mIsSplashShown = false;
    }

    public TimerService getService() {
        return mTimerService;
    }


    public void showSplash() {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(SplashFragment.TAG);
        if (fragment == null) {
            fragment = SplashFragment.newInstance(this);
            getSupportFragmentManager()
                    .beginTransaction()
                    .disallowAddToBackStack()
                    .replace(R.id.fragment_container, fragment, SplashFragment.TAG)
                    .commit();
        } else {
            getSupportFragmentManager().beginTransaction().attach(fragment).commit();
        }

        mToolbar.setVisibility(View.GONE);
    }


    public void showCatalog() {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(CatalogFragment.TAG);
        if (fragment == null) {
            fragment = CatalogFragment.newInstance();
            getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left)
                    .replace(R.id.fragment_container, fragment, CatalogFragment.TAG)
                    .commit();
        } else {
            getSupportFragmentManager().beginTransaction().attach(fragment).commit();
        }

        Animation animation = AnimationUtils.loadAnimation(this, R.anim.enter_from_left);
        mToolbar.startAnimation(animation);
        mToolbar.setVisibility(View.VISIBLE);

    }

    public void showRunningTimer(Timer timer) {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(RunningTimerFragment.TAG);
        if (fragment == null) {
            fragment = RunningTimerFragment.newInstance(timer);

            getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left)
                    .replace(R.id.fragment_container, fragment, RunningTimerFragment.TAG)
                    .commit();
        } else {
            getSupportFragmentManager().beginTransaction().attach(fragment).commit();
        }
    }

    public void showTimerBuilder(Timer timer) {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(AssembleFragment.TAG);
        if (fragment == null) {
            fragment = AssembleFragment.newInstance();
            ((AssembleFragment) fragment).setTimer(timer);
            getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left)
                    .replace(R.id.fragment_container, fragment, AssembleFragment.TAG)
                    .addToBackStack(null)
                    .commit();
        } else {
            ((AssembleFragment) fragment).setTimer(timer);
            getSupportFragmentManager().beginTransaction().attach(fragment).commit();
        }
    }

    public void showSettings() {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(SettingsFragment.TAG);
        if (fragment == null) {
            fragment = SettingsFragment.newInstance();
            getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left)
                    .replace(R.id.fragment_container, fragment, SettingsFragment.TAG)
                    .addToBackStack(null)
                    .commit();
        } else {
            getSupportFragmentManager().beginTransaction().attach(fragment).commit();
        }

    }

    public void showOpenSourceLibraries() {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(LicencesFragment.TAG);
        if (fragment == null) {
            fragment = LicencesFragment.newInstance();
            getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left)
                    .replace(R.id.fragment_container, fragment, LicencesFragment.TAG)
                    .addToBackStack(null)
                    .commit();
        } else {
            getSupportFragmentManager().beginTransaction().attach(fragment).commit();
        }

    }

    @Override
    public void onSplashShown() {
        mIsSplashShown = true;
        navigateAfterStarted();
    }

    private void navigateAfterStarted() {
        if (mIsSplashShown && mIsServiceBound) {
            if (mTimerService.getRunningTimer() != null) {
                showRunningTimer(mTimerService.getRunningTimer());
            } else {
                showCatalog();
            }
        }
    }
}
