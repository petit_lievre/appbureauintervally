package fi.appbureau.intervally.activity.splash;


import android.animation.Animator;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.BounceInterpolator;

import fi.appbureau.intervally.R;
import fi.appbureau.intervally.model.Timer;
import fi.appbureau.intervally.model.TimerDaoHelper;
import fi.appbureau.intervally.preferences.IntervallyPreferences;


public class SplashFragment extends Fragment {

    public static final String TAG = SplashFragment.class.getSimpleName();

    public interface OnSplashShownListener {
        void onSplashShown();
    }

    private OnSplashShownListener mListener;

    public SplashFragment() {
        // Required empty public constructor
    }

    public static SplashFragment newInstance(OnSplashShownListener listener) {
        SplashFragment fragment = new SplashFragment();
        fragment.mListener = listener;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_splash, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Context context = SplashFragment.this.getActivity() != null
                ? SplashFragment.this.getActivity()
                : SplashFragment.this.getContext();
        if (context instanceof OnSplashShownListener) {
            mListener = (OnSplashShownListener) context;
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        addDefaultTimers();

        final View logo = view.findViewById(R.id.logo);

        logo.animate()
                .setDuration(1000)
                .scaleX(2.0f)
                .scaleY(2.0f)
                .setInterpolator(new BounceInterpolator())
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {}

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        logo.setVisibility(View.GONE);
                        mListener.onSplashShown();
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {}

                    @Override
                    public void onAnimationRepeat(Animator animation) {}
                })
                .start();
    }

    private void addDefaultTimers() {
        Context context = SplashFragment.this.getActivity() != null
                ? SplashFragment.this.getActivity()
                : SplashFragment.this.getContext();
        IntervallyPreferences preferences = new IntervallyPreferences(context);
        if (preferences.isFirstLaunch()) {
            Timer timer = new Timer();
            timer.setName(getString(R.string.tabata_timer));
            timer.setRounds(3);
            timer.setNumberOfExercises(8);
            timer.setExercise(20);
            timer.setRest(10);
            timer.setRecovery(60);
            TimerDaoHelper.insertTimer(this, timer);

            timer = new Timer();
            timer.setName(getString(R.string.transformer_timer));
            timer.setRounds(3);
            timer.setNumberOfExercises(8);
            timer.setExercise(45);
            timer.setRest(15);
            timer.setRecovery(60);
            TimerDaoHelper.insertTimer(this, timer);

            timer = new Timer();
            timer.setName(getString(R.string.prformance_timer));
            timer.setRounds(4);
            timer.setNumberOfExercises(6);
            timer.setExercise(90);
            timer.setRest(30);
            timer.setRecovery(120);
            TimerDaoHelper.insertTimer(this, timer);

            timer = new Timer();
            timer.setName(getString(R.string.boxing_timer));
            timer.setWarmUp(180);
            timer.setRounds(6);
            timer.setNumberOfExercises(3);
            timer.setExercise(60);
            timer.setRest(0);
            timer.setRecovery(180);
            timer.setCoolDown(180);
            TimerDaoHelper.insertTimer(this, timer);

            preferences.setIsFirstLaunch(false);
        }
    }
}
