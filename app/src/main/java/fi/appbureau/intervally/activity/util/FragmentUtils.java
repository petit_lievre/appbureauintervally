package fi.appbureau.intervally.activity.util;


import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import fi.appbureau.intervally.R;
import fi.appbureau.intervally.model.Timer;
import fi.appbureau.intervally.model.TimerComponents;
import fi.appbureau.intervally.model.TimerUtils;


public class FragmentUtils {

    public static void configureComponentView(View componentView, Timer timer, int componentId,
                                              @DrawableRes int iconResId, @StringRes int labelResId,
                                              @StringRes int emptyFieldResId) {
        if (iconResId != 0) {
            ImageView iconView = (ImageView) componentView.findViewById(R.id.icon);
            iconView.setImageResource(iconResId);
        }

        TextView labelView = (TextView)componentView.findViewById(R.id.label);
        labelView.setText(labelResId);

        TextView valueView = (TextView)componentView.findViewById(R.id.value);

        Integer duration = null;
        switch (componentId) {
            case TimerComponents.WARM_UP:
                duration = timer.getWarmUp();
                break;
            case TimerComponents.EXERCISE:
                duration = timer.getExercise();
                break;
            case TimerComponents.REST:
                duration = timer.getRest();
                break;
            case TimerComponents.RECOVERY:
                duration = timer.getRecovery();
                break;
            case TimerComponents.NUMBER_OF_EXERCISES:
                duration = timer.getNumberOfExercises();
                break;
            case TimerComponents.ROUNDS:
                duration = timer.getRounds();
                break;
            case TimerComponents.COOL_DOWN:
                duration = timer.getCoolDown();
                break;
            default:
                throw new RuntimeException("Unexpected component id");
        }
        if (duration == null) {
            valueView.setText(emptyFieldResId);
        } else {
            if (componentId == TimerComponents.NUMBER_OF_EXERCISES
                    || componentId == TimerComponents.ROUNDS) {
                valueView.setText(duration.toString());
            } else {
                valueView.setText(TimerUtils.convertDurationToString(duration));
            }
        }


    }

    public static void finish(Fragment fragment) {
        AppCompatActivity activity = (AppCompatActivity)fragment.getActivity();
        if (activity != null) {
            activity.getSupportFragmentManager().beginTransaction().remove(fragment).commit();
            activity.getSupportFragmentManager().popBackStack();
        }
    }
}
