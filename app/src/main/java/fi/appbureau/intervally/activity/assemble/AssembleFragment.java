package fi.appbureau.intervally.activity.assemble;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.codetroopers.betterpickers.hmspicker.HmsPickerBuilder;
import com.codetroopers.betterpickers.hmspicker.HmsPickerDialogFragment;
import com.codetroopers.betterpickers.numberpicker.NumberPickerBuilder;
import com.codetroopers.betterpickers.numberpicker.NumberPickerDialogFragment;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Locale;

import fi.appbureau.intervally.R;
import fi.appbureau.intervally.activity.util.FragmentUtils;
import fi.appbureau.intervally.model.Timer;
import fi.appbureau.intervally.model.TimerComponents;
import fi.appbureau.intervally.model.TimerDaoHelper;
import fi.appbureau.intervally.model.TimerUtils;


public class AssembleFragment extends Fragment {

    public static final String TAG = AssembleFragment.class.getSimpleName();

    private Timer mTimer;

    private View mRootView;
    private EditText mNameField;
    private TextView mNumberOdExercisesValue;
    private TextView mRoundsValue;

    private enum SanityCheckResult {
        UNDEFINED, CORRECT, MISSING_EXERCISE, MISSING_NUMBER_OF_EXERCISES, MISSING_ROUNDS
    }

    public AssembleFragment() {
        // Required empty public constructor
    }

    public static AssembleFragment newInstance() {
        return new AssembleFragment();
    }

    public void setTimer(@Nullable Timer timer) {
        mTimer = timer;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.fragment_assemble, container, false);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_assemble, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_done:
                String name = mNameField.getText().toString();
                if (name == null || "".equals(name)) {
                    showErrorSnackbar(R.string.name_undefined_error);
                } else {
                    SanityCheckResult sanityCheckResult = isIntervalComplete();
                    if (sanityCheckResult != SanityCheckResult.CORRECT) {
                        if (sanityCheckResult == SanityCheckResult.MISSING_EXERCISE) {
                            showErrorSnackbar(R.string.exercise_duration_error);
                        } else if (sanityCheckResult == SanityCheckResult.MISSING_NUMBER_OF_EXERCISES) {
                            showErrorSnackbar(R.string.number_of_exercise_error);
                        } else if (sanityCheckResult == SanityCheckResult.MISSING_ROUNDS) {
                            showErrorSnackbar(R.string.rounds_error);
                        } else if (sanityCheckResult == SanityCheckResult.UNDEFINED) {
                            showErrorSnackbar(R.string.rounds_error);
                        }
                    } else {
                        mTimer.setName(name);
                        TimerDaoHelper.insertTimer(this, mTimer);
                        FragmentUtils.finish(this);
                    }
                }
                return true;
            case android.R.id.home:
                FragmentUtils.finish(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        AppCompatActivity activity = (AppCompatActivity)getActivity();
        if (activity != null) {
            ActionBar actionBar = activity.getSupportActionBar();
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_close);
            actionBar.setDisplayOptions(0, ActionBar.DISPLAY_USE_LOGO);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mRootView = view.findViewById(R.id.root);

        mNameField = (EditText) view.findViewById(R.id.name);
        mNameField.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mNameField.setCursorVisible(true);
            }
        });

        if (mTimer.getName() != null) {
            mNameField.setText(mTimer.getName());
        }

        View componentView = view.findViewById(R.id.warm_up);
        setUpView(componentView, TimerComponents.WARM_UP, R.drawable.ic_warmup,
                R.string.warm_up, R.string.empty_field_time);

        componentView = view.findViewById(R.id.exercise);
        setUpView(componentView, TimerComponents.EXERCISE, R.drawable.ic_exercise,
                R.string.exercise, R.string.empty_field_time);

        componentView = view.findViewById(R.id.rest);
        setUpView(componentView, TimerComponents.REST, 0, R.string.rest, R.string.empty_field_time);

        componentView = view.findViewById(R.id.recovery);
        setUpView(componentView, TimerComponents.RECOVERY, 0, R.string.recovery,
                R.string.empty_field_time);

        componentView = view.findViewById(R.id.numberOfExercises);
        setUpView(componentView, TimerComponents.NUMBER_OF_EXERCISES, 0,
                R.string.number_of_exercises, R.string.empty_field_number);

        componentView = view.findViewById(R.id.rounds);
        setUpView(componentView, TimerComponents.ROUNDS, 0,
                R.string.rounds, R.string.empty_field_number);

        componentView = view.findViewById(R.id.coolDown);
        setUpView(componentView, TimerComponents.COOL_DOWN, R.drawable.ic_cooldown,
                R.string.cool_down, R.string.empty_field_time);
    }

    private void setUpView(View view, int timerComponent, @DrawableRes int icon, @StringRes int label,
                           @StringRes int emptyFieldLabel) {
        TextView valueView = (TextView)view.findViewById(R.id.value);
        FragmentUtils.configureComponentView(view, mTimer, timerComponent, icon, label, emptyFieldLabel);
        configureOnClickActionForView(view, valueView, timerComponent);

        if (timerComponent == TimerComponents.NUMBER_OF_EXERCISES) {
            mNumberOdExercisesValue = valueView;
        }

        if (timerComponent == TimerComponents.ROUNDS) {
            mRoundsValue = valueView;
        }
    }

    private void configureOnClickActionForView(View view, final TextView valueView,
                                               final int updatedTimerComponentId) {
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mNameField.setCursorVisible(false);
                if (updatedTimerComponentId == TimerComponents.NUMBER_OF_EXERCISES
                        || updatedTimerComponentId == TimerComponents.ROUNDS) {

                    NumberPickerBuilder npb = new NumberPickerBuilder()
                            .setFragmentManager(getFragmentManager())
                            .setPlusMinusVisibility(View.GONE)
                            .setDecimalVisibility(View.GONE)
                            .setCurrentNumber(TimerUtils.getComponentValue(mTimer, updatedTimerComponentId))
                            .setStyleResId(R.style.TimeInputTheme);
                    npb.addNumberPickerDialogHandler(new NumberPickerDialogFragment.NumberPickerDialogHandlerV2() {
                        @Override
                        public void onDialogNumberSet(int reference, BigInteger number,
                                                      double decimal, boolean isNegative,
                                                      BigDecimal fullNumber) {
                            if (number.intValue() >= 1) {
                                TimerUtils.updateComponentValue(mTimer, updatedTimerComponentId,
                                        number.intValue());
                                valueView.setText(String.format(Locale.US, "%d", number.intValue()));
                            } else {
                                if (updatedTimerComponentId == TimerComponents.NUMBER_OF_EXERCISES) {
                                    showErrorSnackbar(R.string.number_of_exercise_error);
                                } else {
                                    showErrorSnackbar(R.string.rounds_error);
                                }
                            }
                        }
                    });
                    npb.show();

                } else {
                    HmsPickerBuilder hpb = new HmsPickerBuilder()
                            .setFragmentManager(getFragmentManager())
                            .setTimeInSeconds(TimerUtils.getComponentValue(mTimer, updatedTimerComponentId))
                            .setStyleResId(R.style.TimeInputTheme);
                    hpb.addHmsPickerDialogHandler(new HmsPickerDialogFragment.HmsPickerDialogHandlerV2() {

                        @Override
                        public void onDialogHmsSet(int reference, boolean isNegative, int hours,
                                                   int minutes, int seconds) {
                            int durationInSeconds = hours * 60 * 60 + minutes * 60 + seconds;
                            if (updatedTimerComponentId == TimerComponents.EXERCISE && durationInSeconds < 1) {
                                showErrorSnackbar(R.string.exercise_duration_error);
                            } else {
                                TimerUtils.updateComponentValue(mTimer, updatedTimerComponentId,
                                        durationInSeconds);

                                valueView.setText(TimerUtils.convertDurationToString(durationInSeconds));

                                if (updatedTimerComponentId == TimerComponents.EXERCISE) {
                                    if (mTimer.getNumberOfExercises() == null || mTimer.getNumberOfExercises() == 0) {
                                        mTimer.setNumberOfExercises(1);
                                    }

                                    if (mTimer.getRounds() == null || mTimer.getRounds() == 0) {
                                        mTimer.setRounds(1);
                                    }

                                    mNumberOdExercisesValue.setText(mTimer.getNumberOfExercises().toString());
                                    mRoundsValue.setText(mTimer.getRounds().toString());
                                }
                            }
                        }
                    });
                    hpb.show();
                }
            }
        });
    }

    private void showErrorSnackbar(@StringRes int explanation) {
        Context context = getActivity() != null ? getActivity() : getContext();
        Snackbar snackbar = Snackbar.make(mRootView, explanation, Snackbar.LENGTH_SHORT);
        snackbar.getView().setBackgroundColor(ContextCompat.getColor(context, R.color.colorSnackbarError));
        snackbar.show();
    }

    private SanityCheckResult isIntervalComplete() {
        SanityCheckResult result = SanityCheckResult.UNDEFINED;

        if (((mTimer.getWarmUp() != null && mTimer.getWarmUp() > 0)
                || (mTimer.getCoolDown() != null && mTimer.getCoolDown() > 0))
                && (mTimer.getExercise() == null || mTimer.getExercise() == 0)) {
            result = SanityCheckResult.CORRECT;
        } else {
            if (mTimer.getExercise() == null || mTimer.getExercise() == 0) {
                result = SanityCheckResult.MISSING_EXERCISE;
            } else if (mTimer.getNumberOfExercises() == null || mTimer.getNumberOfExercises() == 0) {
                result = SanityCheckResult.MISSING_NUMBER_OF_EXERCISES;
            } else if (mTimer.getRounds() == null || mTimer.getRounds() == 0) {
                result = SanityCheckResult.MISSING_ROUNDS;
            } else {
                result = SanityCheckResult.CORRECT;
            }
        }

        return result;
    }
}
