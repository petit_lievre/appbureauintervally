package fi.appbureau.intervally.activity.settings;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import fi.appbureau.intervally.R;
import fi.appbureau.intervally.activity.util.FragmentUtils;


public class LicencesFragment extends Fragment {

    public static final String TAG = LicencesFragment.class.getSimpleName();

    public LicencesFragment() {
        // Required empty public constructor
    }

    public static LicencesFragment newInstance() {
        return new LicencesFragment();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.fragment_licences, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();

        AppCompatActivity activity = (AppCompatActivity)getActivity();
        if (activity != null) {
            ActionBar actionBar = activity.getSupportActionBar();
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back);
            actionBar.setDisplayOptions(0, ActionBar.DISPLAY_USE_LOGO);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                FragmentUtils.finish(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        TextView textView = (TextView) view.findViewById(R.id.lib_dao_core);
        textView.setMovementMethod(LinkMovementMethod.getInstance());

        textView = (TextView) view.findViewById(R.id.appache_licence_v_2);
        textView.setMovementMethod(LinkMovementMethod.getInstance());

        textView = (TextView) view.findViewById(R.id.lib_better_pickers);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
    }
}
