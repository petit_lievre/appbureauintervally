package fi.appbureau.intervally.activity.settings;


import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;

import java.util.Locale;

import fi.appbureau.intervally.R;
import fi.appbureau.intervally.activity.IntervallyActivity;
import fi.appbureau.intervally.activity.util.FragmentUtils;
import fi.appbureau.intervally.model.TimerComponents;
import fi.appbureau.intervally.preferences.IntervallyPreferences;


public class SettingsFragment extends Fragment {

    public static final String TAG = SettingsFragment.class.getSimpleName();

    private IntervallyPreferences mPreferences;

    public SettingsFragment() {
        // Required empty public constructor
    }

    public static SettingsFragment newInstance() {
        return new SettingsFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Context context = getActivity() != null ? getActivity() : getContext();
        mPreferences = new IntervallyPreferences(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.fragment_settings, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();

        AppCompatActivity activity = (AppCompatActivity)getActivity();
        if (activity != null) {
            ActionBar actionBar = activity.getSupportActionBar();
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back);
            actionBar.setDisplayOptions(0, ActionBar.DISPLAY_USE_LOGO);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                FragmentUtils.finish(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Context context = getActivity() != null ? getActivity() : getContext();
        final AudioManager audioManager = (AudioManager)context.getSystemService(Context.AUDIO_SERVICE);
        int maxVolumeLevel = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);

        SeekBar seekbar = (SeekBar)view.findViewById(R.id.slider_warm_up);
        TextView value = (TextView)view.findViewById(R.id.warm_up_value);
        configureVolumeLevelView(seekbar, value, TimerComponents.WARM_UP, maxVolumeLevel);

        seekbar = (SeekBar)view.findViewById(R.id.slider_exercise);
        value = (TextView)view.findViewById(R.id.exercise_volume_value);
        configureVolumeLevelView(seekbar, value, TimerComponents.EXERCISE, maxVolumeLevel);

        seekbar = (SeekBar)view.findViewById(R.id.slider_rest);
        value = (TextView)view.findViewById(R.id.rest_volume_value);
        configureVolumeLevelView(seekbar, value, TimerComponents.REST, maxVolumeLevel);

        seekbar = (SeekBar)view.findViewById(R.id.slider_recovery);
        value = (TextView)view.findViewById(R.id.recovery_volume_value);
        configureVolumeLevelView(seekbar, value, TimerComponents.RECOVERY, maxVolumeLevel);

        seekbar = (SeekBar)view.findViewById(R.id.slider_cool_down);
        value = (TextView)view.findViewById(R.id.cool_down_volume_value);
        configureVolumeLevelView(seekbar, value, TimerComponents.COOL_DOWN, maxVolumeLevel);

        Switch playSoundSwitch = (Switch)view.findViewById(R.id.switch_play_sounds);
        playSoundSwitch.setChecked(mPreferences.getPlaySounds());
        playSoundSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mPreferences.setPlaySounds(isChecked);
            }
        });

        view.findViewById(R.id.report_problem).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto","intervally.issues@gmail.com", null));
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject");
                emailIntent.putExtra(Intent.EXTRA_TEXT, "Body");
                startActivity(Intent.createChooser(emailIntent, getString(R.string.send_problems_title)));
            }
        });

        view.findViewById(R.id.rate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context context = getActivity() != null ? getActivity() : getContext();
                context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + context.getPackageName())));
            }
        });

        view.findViewById(R.id.open_source_libraries).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null) {
                    IntervallyActivity activity = (IntervallyActivity) getActivity();
                    activity.showOpenSourceLibraries();
                }
            }
        });
    }

    private void configureVolumeLevelView(SeekBar seekbar, final TextView value, final int timerComponent,
                                          int maxVolumeLevel) {
        value.setText(String.format(Locale.US, "%d", mPreferences.getVolumeLevelForComponent(timerComponent)));
        seekbar.setMax(maxVolumeLevel);
        seekbar.setProgress(mPreferences.getVolumeLevelForComponent(timerComponent));
        seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                seekBar.setProgress(progress);
                value.setText(String.format(Locale.US, "%d", progress));
                mPreferences.setVolumeLevelForComponent(timerComponent, progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                SettingsFragment.this.playFeedbackSound(mPreferences.getVolumeLevelForComponent(timerComponent));
            }
        });
    }

    private void playFeedbackSound(int level) {
        Context context = getActivity() != null ? getActivity() : getContext();
        final AudioManager audioManager =
                (AudioManager)context.getSystemService(Context.AUDIO_SERVICE);
        final int volume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        int alert = R.raw.beep;

        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
                level, AudioManager.FLAG_VIBRATE);

        final MediaPlayer mediaPlayer = MediaPlayer.create(context.getApplicationContext(), alert);
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mediaPlayer.release();
                audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
                        volume, AudioManager.FLAG_VIBRATE);
            }
        });
        mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mediaPlayer.start();
            }
        });
    }
}
