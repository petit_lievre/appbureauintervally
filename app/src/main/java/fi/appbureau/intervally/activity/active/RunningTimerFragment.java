package fi.appbureau.intervally.activity.active;


import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;

import java.io.Serializable;

import fi.appbureau.intervally.R;
import fi.appbureau.intervally.activity.IntervallyActivity;
import fi.appbureau.intervally.analytics.IntervallyAnalytics;
import fi.appbureau.intervally.model.Timer;
import fi.appbureau.intervally.model.TimerHelper;
import fi.appbureau.intervally.model.TimerUtils;
import fi.appbureau.intervally.preferences.IntervallyPreferences;
import fi.appbureau.intervally.service.TimerDelegate;
import fi.appbureau.intervally.service.TimerService;


public class RunningTimerFragment extends Fragment implements TimerDelegate {

    public static final String TAG = RunningTimerFragment.class.getSimpleName();

    private static final String TIMER_KEY = "timer";
    private static final int DEFAULT_INITIAL_DELAY = 5;
    private static final int DEFAULT_ANIMATION_DURATION = 300;

    private Timer mTimer;

    private View mProgressView;
    private View mHelpView;
    private TextView mCountDownView;
    private ImageButton mToggleButton;
    private ImageButton mStopButton;
    private TextView mTotalLeft;
    private TextView mPhaseLeft;
    private TextView mPhase;
    private ProgressBar mTotalProgress;
    private ProgressBar mPhaseProgress;

    private IntervallyActivity mIntervallyActivity;

    @Nullable
    private TimerService mTimerService;

    @Override
    public void onActiveTimerChanged(long total, long totalLeft, long interval, long intervalLeft, int type) {
        updateViewsVisibility(View.GONE, View.VISIBLE);
        updateProgressViews((int) total, (int) totalLeft, (int) interval, (int) intervalLeft, type);
        mToggleButton.setBackgroundResource(R.drawable.ic_pause);
    }

    @Override
    public void onActiveTimerStartScheduled(int timeTillStart) {
        updateViewsVisibility(View.VISIBLE, View.GONE);
        if (timeTillStart == 0) {
            mCountDownView.setText(getString(R.string.start_workout));
        } else {
            mCountDownView.setText("" + timeTillStart);
        }
    }

    @Override
    public void onActiveTimerStarted(long total, long interval) {
        mTotalProgress.setMax((int) total);
        mPhaseProgress.setMax((int) interval);
        mToggleButton.setBackgroundResource(R.drawable.ic_pause);
    }

    @Override
    public void onActiveTimerPaused() {
        mToggleButton.setBackgroundResource(R.drawable.ic_play);
    }

    @Override
    public void onActiveTimerStopped() {
        if (mIntervallyActivity != null) {
            mIntervallyActivity.showCatalog();
        }
    }

    public RunningTimerFragment() {
        // Required empty public constructor
    }

    public static RunningTimerFragment newInstance(Timer timer) {
        RunningTimerFragment fragment = new RunningTimerFragment();
        fragment.mTimer = timer;
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Context context = getActivity() != null ? getActivity() : getContext();

        if (context instanceof IntervallyActivity) {
            mIntervallyActivity = (IntervallyActivity) context;
            if (mIntervallyActivity.getService() != null) {
                mTimerService = mIntervallyActivity.getService();
                mTimerService.addActiveTimerDelegate(this);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        updateViewsVisibility(View.GONE, View.GONE);

        if (mTimerService != null) {
            if (mTimerService.getState() == TimerService.PAUSED_STATE) {
                updateViewsVisibility(View.GONE, View.VISIBLE);
                updateProgressViews(mTimerService.getTotal(), mTimerService.getTotalLeft(),
                        mTimerService.getInterval(), mTimerService.getIntervalLeft(),
                        mTimerService.getPhase());
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_running_timer, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        deserializeTimerIfNeeded(savedInstanceState);
        uploadAnalytics();
        configureViews(view);
        startTimerIfNeeded();
    }

    private void startTimerIfNeeded() {
        if (mTimerService != null
                && (mTimerService.getState() != TimerService.RUNNING_STATE
                && mTimerService.getState() != TimerService.SCHEDULED_STATE
                && mTimerService.getState() != TimerService.PAUSED_STATE)) {
            mTimerService.startTimer(mTimer, DEFAULT_INITIAL_DELAY);
        }
    }

    private void configureViews(View view) {
        TextView name = (TextView) view.findViewById(R.id.title);
        name.setText(mTimer.getName());

        mProgressView = view.findViewById(R.id.progress);
        mHelpView = view.findViewById(R.id.help);
        mCountDownView = (TextView) view.findViewById(R.id.countDown);
        mToggleButton = (ImageButton) view.findViewById(R.id.pause);
        mStopButton = (ImageButton) view.findViewById(R.id.stop);

        mToggleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mTimerService != null
                        && mTimerService.getState() == TimerService.RUNNING_STATE) {
                    mToggleButton.setBackgroundResource(R.drawable.ic_play);
                    mTimerService.pauseTimer();
                } else if (mTimerService != null
                        && mTimerService.getState() == TimerService.PAUSED_STATE) {
                    mToggleButton.setBackgroundResource(R.drawable.ic_pause);
                    mTimerService.doContinueActiveTimer();
                }
            }
        });

        mStopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mTimerService != null) {
                    mTimerService.stopTimer();
                }
            }
        });

        mTotalLeft = (TextView) view.findViewById(R.id.totalLeft);
        mPhaseLeft = (TextView) view.findViewById(R.id.phaseLeft);
        mPhase = (TextView) view.findViewById(R.id.phase);
        mTotalProgress = (ProgressBar) view.findViewById(R.id.totalProgress);
        mPhaseProgress = (ProgressBar) view.findViewById(R.id.phaseProgress);
    }

    private void deserializeTimerIfNeeded(@Nullable Bundle savedInstanceState) {
        if (mTimer == null && savedInstanceState != null) {
            Serializable serializableTimer = savedInstanceState.getSerializable(TIMER_KEY);
            if (serializableTimer != null) {
                mTimer = (Timer) serializableTimer;
            }
        }
    }

    private void uploadAnalytics() {
        Context context = getActivity() != null ? getActivity() : getContext();

        FirebaseAnalytics firebaseAnalytics = FirebaseAnalytics.getInstance(context);

        IntervallyPreferences preferences = new IntervallyPreferences(context);
        Bundle bundle = new Bundle();
        bundle.putString(IntervallyAnalytics.PARAM_TIMER, TimerHelper.toAnalyticsParam(mTimer));
        bundle.putString(IntervallyAnalytics.PARAM_SETTINGS, preferences.toAnalyticsParam());

        firebaseAnalytics.logEvent(IntervallyAnalytics.EVENT_STARTED_TIMER, bundle);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (mTimerService != null) {
            mTimerService.removeActiveTimerDelegate(this);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(TIMER_KEY, mTimer);
    }

    private void updateViewsVisibility(int showHelpViews, int showProgressViews) {
        mHelpView.setVisibility(showHelpViews);
        mCountDownView.setVisibility(showHelpViews);

        mProgressView.setVisibility(showProgressViews);
        mToggleButton.setVisibility(showProgressViews);
        mStopButton.setVisibility(showProgressViews);
    }

    private void updateProgressViews(int total, int totalLeft, int interval, int intervalLeft, int type) {
        mTotalProgress.setMax(total);
        animateProgressChange(mTotalProgress, mTotalProgress.getProgress(), totalLeft);

        mPhaseProgress.setMax(interval);
        animateProgressChange(mPhaseProgress, mPhaseProgress.getProgress(), intervalLeft);

        mTotalLeft.setText(TimerUtils.convertDurationToString(totalLeft));
        mPhaseLeft.setText(TimerUtils.convertDurationToString(intervalLeft));

        Context context = getActivity() != null ? getActivity() : getContext();
        if (context != null) {
            mPhase.setText(TimerUtils.getPhaseName(context, type));
        }
    }

    private void animateProgressChange(final ProgressBar progressBar, final int fromValue, final int toValue) {
        ObjectAnimator animation = ObjectAnimator.ofInt(progressBar, "progress", toValue);
        animation.setDuration(DEFAULT_ANIMATION_DURATION);
        animation.setInterpolator(new DecelerateInterpolator());
        animation.start();
        animation.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                progressBar.setProgress(fromValue);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                progressBar.setProgress(toValue);
            }

            @Override
            public void onAnimationCancel(Animator animation) {}

            @Override
            public void onAnimationRepeat(Animator animation) {}
        });
    }
}
