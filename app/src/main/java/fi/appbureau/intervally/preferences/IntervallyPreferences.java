package fi.appbureau.intervally.preferences;


import android.content.Context;
import android.content.SharedPreferences;

import fi.appbureau.intervally.model.TimerComponents;

public class IntervallyPreferences {

    private static final String PREFERENCES_NAME = IntervallyPreferences.class.getSimpleName();

    private static final String IS_FIRST_LAUNCH = "IsFirstLaunch";
    private static final boolean IS_FIRST_LAUNCH_DEFAULT = true;

    private static final String WARM_UP_VOLUME_LEVEL_NAME = "WARM_UP_VOLUME_LEVEL";
    private static final int WARM_UP_VOLUME_LEVEL_DEFAULT = 6;

    private static final String EXERCISE_VOLUME_LEVEL_NAME = "EXERCISE_VOLUME_LEVEL";
    private static final int EXERCISE_VOLUME_LEVEL_DEFAULT = 8;

    private static final String REST_VOLUME_LEVEL_NAME = "REST_VOLUME_LEVEL";
    private static final int REST_VOLUME_LEVEL_DEFAULT = 4;

    private static final String RECOVERY_VOLUME_LEVEL_NAME = "RECOVERY_VOLUME_LEVEL";
    private static final int RECOVERY_VOLUME_LEVEL_DEFAULT = 3;

    private static final String COOL_DOWN_VOLUME_LEVEL_NAME = "COOL_DOWN_VOLUME_LEVEL";
    private static final int COOL_DOWN_VOLUME_LEVEL_DEFAULT = 6;

    private static final String PLAY_SOUNDS_NAME = "PLAY_SOUNDS";
    private static final boolean PLAY_SOUNDS_DEFAULT = true;

    private final SharedPreferences preferences;

    public IntervallyPreferences(Context context) {
        this.preferences = context.getApplicationContext().getSharedPreferences(PREFERENCES_NAME,
                Context.MODE_PRIVATE);
    }

    public int getWarmUpVolumeLevel() {
        return preferences.getInt(WARM_UP_VOLUME_LEVEL_NAME, WARM_UP_VOLUME_LEVEL_DEFAULT);
    }

    private void setWarmUpVolumeLevel(int level) {
        preferences.edit().putInt(WARM_UP_VOLUME_LEVEL_NAME, level).apply();
    }

    public int getExerciseVolumeLevel() {
        return preferences.getInt(EXERCISE_VOLUME_LEVEL_NAME, EXERCISE_VOLUME_LEVEL_DEFAULT);
    }

    private void setExerciseVolumeLevel(int level) {
        preferences.edit().putInt(EXERCISE_VOLUME_LEVEL_NAME, level).apply();
    }

    public int getRestVolumeLevel() {
        return preferences.getInt(REST_VOLUME_LEVEL_NAME, REST_VOLUME_LEVEL_DEFAULT);
    }

    private void setRestVolumeLevel(int level) {
        preferences.edit().putInt(REST_VOLUME_LEVEL_NAME, level).apply();
    }

    public int getRecoveryVolumeLevel() {
        return preferences.getInt(RECOVERY_VOLUME_LEVEL_NAME, RECOVERY_VOLUME_LEVEL_DEFAULT);
    }

    public void setRecoveryVolumeLevel(int level) {
        preferences.edit().putInt(RECOVERY_VOLUME_LEVEL_NAME, level).apply();
    }

    public int getCoolDownVolumeLevel() {
        return preferences.getInt(COOL_DOWN_VOLUME_LEVEL_NAME, COOL_DOWN_VOLUME_LEVEL_DEFAULT);
    }

    private void setCoolDownVolumeLevel(int level) {
        preferences.edit().putInt(COOL_DOWN_VOLUME_LEVEL_NAME, level).apply();
    }

    public int getVolumeLevelForComponent(int timerComponent) {
        int volumeLevel = 0;
        switch (timerComponent) {
            case TimerComponents.WARM_UP:
                volumeLevel = getWarmUpVolumeLevel();
                break;
            case TimerComponents.EXERCISE:
                volumeLevel = getExerciseVolumeLevel();
                break;
            case TimerComponents.REST:
                volumeLevel = getRestVolumeLevel();
                break;
            case TimerComponents.RECOVERY:
                volumeLevel = getRecoveryVolumeLevel();
                break;
            case TimerComponents.COOL_DOWN:
                volumeLevel = getCoolDownVolumeLevel();
                break;
            default:
                throw new RuntimeException("Unexpected component id");
        }
        return volumeLevel;
    }

    public void setVolumeLevelForComponent(int timerComponent, int volumeLevel) {
        switch (timerComponent) {
            case TimerComponents.WARM_UP:
                setWarmUpVolumeLevel(volumeLevel);
                break;
            case TimerComponents.EXERCISE:
                setExerciseVolumeLevel(volumeLevel);
                break;
            case TimerComponents.REST:
                setRestVolumeLevel(volumeLevel);
                break;
            case TimerComponents.RECOVERY:
                setRecoveryVolumeLevel(volumeLevel);
                break;
            case TimerComponents.COOL_DOWN:
                setCoolDownVolumeLevel(volumeLevel);
                break;
            default:
                throw new RuntimeException("Unexpected component id");
        }
    }


    public boolean getPlaySounds() {
        return preferences.getBoolean(PLAY_SOUNDS_NAME, PLAY_SOUNDS_DEFAULT);
    }

    public void setPlaySounds(boolean playSounds) {
        preferences.edit().putBoolean(PLAY_SOUNDS_NAME, playSounds).apply();
    }

    public boolean isFirstLaunch() {
        return preferences.getBoolean(IS_FIRST_LAUNCH, IS_FIRST_LAUNCH_DEFAULT);
    }

    public void setIsFirstLaunch(boolean isFirstLaunch) {
        preferences.edit().putBoolean(IS_FIRST_LAUNCH, isFirstLaunch).apply();
    }

    public String toAnalyticsParam() {
        return WARM_UP_VOLUME_LEVEL_NAME +
                "=" +
                preferences.getInt(WARM_UP_VOLUME_LEVEL_NAME, WARM_UP_VOLUME_LEVEL_DEFAULT) +
                ";" +
                EXERCISE_VOLUME_LEVEL_NAME +
                "=" +
                preferences.getInt(EXERCISE_VOLUME_LEVEL_NAME, EXERCISE_VOLUME_LEVEL_DEFAULT) +
                ";" +
                REST_VOLUME_LEVEL_NAME +
                "=" +
                preferences.getInt(REST_VOLUME_LEVEL_NAME, REST_VOLUME_LEVEL_DEFAULT) +
                ";" +
                RECOVERY_VOLUME_LEVEL_NAME +
                "=" +
                preferences.getInt(RECOVERY_VOLUME_LEVEL_NAME, RECOVERY_VOLUME_LEVEL_DEFAULT) +
                ";" +
                COOL_DOWN_VOLUME_LEVEL_NAME +
                "=" +
                preferences.getInt(COOL_DOWN_VOLUME_LEVEL_NAME, COOL_DOWN_VOLUME_LEVEL_DEFAULT) +
                ";" +
                PLAY_SOUNDS_NAME +
                "=" +
                preferences.getBoolean(PLAY_SOUNDS_NAME, PLAY_SOUNDS_DEFAULT) +
                ";";
    }
}
