package fi.appbureau.intervally.notification;


import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.widget.RemoteViews;

import fi.appbureau.intervally.R;
import fi.appbureau.intervally.activity.IntervallyActivity;
import fi.appbureau.intervally.model.TimerUtils;
import fi.appbureau.intervally.service.TimerDelegate;
import fi.appbureau.intervally.service.TimerService;


public class RunningTimerNotification implements TimerDelegate {

    private final Context mContext;

    @Nullable private TimerService mTimerService;

    public RunningTimerNotification(Context context) {
        mContext = context;

        Intent intent = new Intent(mContext, TimerService.class);
        ServiceConnection connection = new ServiceConnection() {

            @Override
            public void onServiceConnected(ComponentName className,
                                           IBinder service) {
                TimerService.LocalBinder binder = (TimerService.LocalBinder) service;
                mTimerService = binder.getService();

                mTimerService.addActiveTimerDelegate(RunningTimerNotification.this);
            }

            @Override
            public void onServiceDisconnected(ComponentName arg0) {
                mTimerService = null;
            }
        };
        mContext.bindService(intent, connection, Context.BIND_AUTO_CREATE);
    }

    private void showNotification(long total, long secondsLeftTotal,
                                  long interval, long secondLeftInterval, int type) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(mContext)
                .setSmallIcon(R.drawable.ic_status_bar).setOngoing(true);
        Intent intent = new Intent(mContext, IntervallyActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(mContext, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(pendingIntent);

        Notification notification = builder.build();

        RemoteViews contentView = new RemoteViews(mContext.getPackageName(), R.layout.notification);
        contentView.setTextViewText(R.id.phase, TimerUtils.getPhaseName(mContext, type));
        contentView.setTextViewText(R.id.phaseLeft, TimerUtils.convertDurationToString((int)secondLeftInterval));
        contentView.setTextViewText(R.id.totalLeft, TimerUtils.convertDurationToString((int)secondsLeftTotal));
        contentView.setProgressBar(R.id.phaseLeftProgress, (int)interval, (int)secondLeftInterval, false);
        contentView.setProgressBar(R.id.phaseTotalProgress, (int)total, (int)secondsLeftTotal, false);
        if (mTimerService!= null && mTimerService.getState() == TimerService.RUNNING_STATE) {
            contentView.setInt(R.id.toggle, "setBackgroundResource", R.drawable.ic_pause);
        } else {
            contentView.setInt(R.id.toggle, "setBackgroundResource", R.drawable.ic_play);
        }

        Intent toggleIntent = new Intent(mContext, TimerService.class);
        toggleIntent.setAction(TimerService.ACTION_TOGGLE);
        PendingIntent togglePendingIntent = PendingIntent.getService(mContext, 0, toggleIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        contentView.setOnClickPendingIntent(R.id.toggle, togglePendingIntent);

        notification.contentView = contentView;

        NotificationManager mNotificationManager =
                (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(0, notification);
    }

    @Override
    public void onActiveTimerChanged(long total, long secondsLeftTotal,
                                     long interval, long secondLeftInterval, int type) {
        showNotification(total, secondsLeftTotal,interval, secondLeftInterval, type);
    }

    @Override
    public void onActiveTimerStartScheduled(int timeTillStart) {

    }

    @Override
    public void onActiveTimerStarted(long secondsLeftTotal, long secondLeftInterval) {
        showNotification(mTimerService.getTotal(), mTimerService.getTotalLeft(),
                mTimerService.getInterval(), mTimerService.getIntervalLeft(),
                mTimerService.getPhase());
    }

    @Override
    public void onActiveTimerPaused() {
        showNotification(mTimerService.getTotal(), mTimerService.getTotalLeft(),
                mTimerService.getInterval(), mTimerService.getIntervalLeft(),
                mTimerService.getPhase());
    }

    @Override
    public void onActiveTimerStopped() {
        NotificationManager mNotificationManager =
                (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.cancel(0);
    }
}
