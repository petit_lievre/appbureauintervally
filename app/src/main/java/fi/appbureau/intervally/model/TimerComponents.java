package fi.appbureau.intervally.model;


public class TimerComponents {

    public static final int WARM_UP = 1;
    public static final int EXERCISE = 2;
    public static final int REST = 3;
    public static final int RECOVERY = 4;
    public static final int NUMBER_OF_EXERCISES = 5;
    public static final int ROUNDS = 6;
    public static final int COOL_DOWN = 7;


}
