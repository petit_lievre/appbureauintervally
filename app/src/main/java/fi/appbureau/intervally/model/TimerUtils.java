package fi.appbureau.intervally.model;


import android.content.Context;

import java.util.Locale;

import fi.appbureau.intervally.R;

public class TimerUtils {

    public static int getComponentValue(Timer timer, int component) {
        int result = 0;
        switch (component) {
            case TimerComponents.WARM_UP: {
                Integer value = timer.getWarmUp();
                result = value != null ? value : 0;
                break;
            }
            case TimerComponents.EXERCISE: {
                Integer value = timer.getExercise();
                result = value != null ? value : 0;
                break;
            }
            case TimerComponents.REST: {
                Integer value = timer.getRest();
                result = value != null ? value : 0;
                break;
            }
            case TimerComponents.RECOVERY: {
                Integer value = timer.getRecovery();
                result = value != null ? value : 0;
                break;
            }
            case TimerComponents.NUMBER_OF_EXERCISES: {
                Integer value = timer.getNumberOfExercises();
                result = value != null ? value : 0;
                break;
            }
            case TimerComponents.ROUNDS: {
                Integer value = timer.getRounds();
                result = value != null ? value : 0;
                break;
            }
            case TimerComponents.COOL_DOWN: {
                Integer value = timer.getCoolDown();
                result = value != null ? value : 0;
                break;
            }
        }

        return result;
    }

    public static void updateComponentValue(Timer timer, int component, int value) {
        switch (component) {
            case TimerComponents.WARM_UP:
                timer.setWarmUp(value);
                break;
            case TimerComponents.EXERCISE:
                timer.setExercise(value);
                break;
            case TimerComponents.REST:
                timer.setRest(value);
                break;
            case TimerComponents.RECOVERY:
                timer.setRecovery(value);
                break;
            case TimerComponents.NUMBER_OF_EXERCISES:
                timer.setNumberOfExercises(value);
                break;
            case TimerComponents.ROUNDS:
                timer.setRounds(value);
                break;
            case TimerComponents.COOL_DOWN:
                timer.setCoolDown(value);
                break;
            default:
                throw new RuntimeException("Unexpected component id");
        }
    }

    public static String convertDurationToString(int durationInSeconds) {
        String duration;
        if ((durationInSeconds / 3600) > 0) {
            duration = String.format(Locale.US, "%d:%02d:%02d", durationInSeconds / 3600,
                    (durationInSeconds % 3600) / 60, durationInSeconds % 60);
        } else {
            duration = String.format(Locale.US, "%02d:%02d", (durationInSeconds % 3600) / 60,
                    durationInSeconds % 60);
        }
        return duration;
    }

    public static String getTotalDuration(Timer timer) {
        int warmUp = timer.getWarmUp() != null ? timer.getWarmUp() : 0;
        int exercise = timer.getExercise() != null ? timer.getExercise() : 0;
        int rest = timer.getRest() != null ? timer.getRest() : 0;
        int recovery = timer.getRecovery() != null ? timer.getRecovery() : 0;
        int numberOfExercises = timer.getNumberOfExercises() != null ? timer.getNumberOfExercises() : 0;
        int rounds = timer.getRounds() != null ? timer.getRounds() : 0;
        int coolDown = timer.getCoolDown() != null ? timer.getCoolDown() : 0;

        int totalDuration = warmUp + (exercise + rest) * numberOfExercises * rounds - rounds * rest + (recovery * rounds) + coolDown;
        return convertDurationToString(totalDuration);
    }

    public static String getPhaseName(Context context, int component) {
        switch (component) {
            case TimerComponents.WARM_UP:
                return context.getString(R.string.warm_up);
            case TimerComponents.EXERCISE:
                return context.getString(R.string.exercise);
            case TimerComponents.REST:
                return context.getString(R.string.rest);
            case TimerComponents.RECOVERY:
                return context.getString(R.string.recovery);
            case TimerComponents.COOL_DOWN:
                return context.getString(R.string.cool_down);
        }
        return "";
    }
}
