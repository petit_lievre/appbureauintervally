package fi.appbureau.intervally.model;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.app.Fragment;

import java.util.List;

public class TimerDaoHelper {

    public static void insertTimer(Context context, Timer timer) {
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(context.getApplicationContext(), "intervally", null);
        SQLiteDatabase db = helper.getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(db);
        TimerDao timerDao = daoMaster.newSession().getTimerDao();
        timerDao.insertOrReplace(timer);
        db.close();
    }

    public static void insertTimer(Fragment fragment, Timer timer) {
        Context context = fragment.getActivity() != null ? fragment.getActivity() : fragment.getContext();
        if (context != null) {
            insertTimer(context, timer);
        }
    }

    public static void deleteTimer(Context context, Timer timer) {
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(context.getApplicationContext(), "intervally", null);
        SQLiteDatabase db = helper.getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(db);
        TimerDao timerDao = daoMaster.newSession().getTimerDao();
        timerDao.delete(timer);
        db.close();
    }

    public static void deleteTimer(Fragment fragment, Timer timer) {
        Context context = fragment.getActivity() != null ? fragment.getActivity() : fragment.getContext();
        if (context != null) {
            deleteTimer(context, timer);
        }
    }

    public static List<Timer> loadAll(Context context) {
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(context.getApplicationContext(), "intervally", null);
        SQLiteDatabase db = helper.getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(db);
        TimerDao timerDao = daoMaster.newSession().getTimerDao();
        List<Timer> result = timerDao.loadAll();
        db.close();
        return result;
    }
}
