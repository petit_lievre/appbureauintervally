package fi.appbureau.intervally.model;


public class TimerHelper {

    public static String toAnalyticsParam(Timer timer) {
        return "NAME" +
                "=" +
                timer.getName() +
                ";" +
                TimerComponents.WARM_UP +
                "=" +
                timer.getWarmUp() +
                ";" +
                TimerComponents.EXERCISE +
                "=" +
                timer.getExercise() +
                ";" +
                TimerComponents.REST +
                "=" +
                timer.getRest() +
                ";" +
                TimerComponents.RECOVERY +
                "=" +
                timer.getRecovery() +
                ";" +
                TimerComponents.NUMBER_OF_EXERCISES +
                "=" +
                timer.getNumberOfExercises() +
                ";" +
                TimerComponents.ROUNDS +
                "=" +
                timer.getRounds() +
                ";" +
                TimerComponents.COOL_DOWN +
                "=" +
                timer.getCoolDown() +
                ";";
    }
}
